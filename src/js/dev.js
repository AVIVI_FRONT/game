function rateStarsActivation() {
    $(document).on('click', '.stars__item', function () {
        $(this).parents('.stars').find('.stars__item').removeClass('active');
        $(this).addClass('active');
    })
}

function watchedSliderInit() {
    $('.js-watched-slider').slick({
        infinite: false,
        initialSlide: 0,
        slidesToShow: 4,
        slidesToScroll: 1,
        prevArrow: '<button class="button button__arrow"></button>',
        nextArrow: '<button class="button button__arrow button__arrow--next"></button>',
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 991,
                settings: {
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1
                }
            }
        ]
    });
    $(window).resize();
}

function graysSliderInit() {
    var slider = $('.js-gray-slider');
    if (slider.length) {
        slider.slick({
            infinite: false,
            initialSlide: 0,
            slidesToShow: 4,
            slidesToScroll: 1,
            prevArrow: '<button class="button button__arrow"></button>',
            nextArrow: '<button class="button button__arrow button__arrow--next"></button>',
            responsive: [
                {
                    breakpoint: 1200,
                    settings: {
                        slidesToShow: 3
                    }
                },
                {
                    breakpoint: 991,
                    settings: {
                        slidesToShow: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1
                    }
                }
            ]
        })
    }
}

function hiderFilter() {
    $(document).on('click', '.hider__head', function (e) {
        e.stopPropagation();
        var button = $(this);
        var field = $(this).parents('.hider').find('.hider__body');
        if (button.hasClass('active')) {
            button.removeClass('active');
            field.removeClass('active');
        } else {
            button.addClass('active');
            field.addClass('active');
        }
        windowCoords();
    });
}

function catalogDisplayer() {
    var filters = $('.js-filters');
    var catalog = $('.catalog');
    var body = $('body');
    $(document).on('click', '.js-catalog-clicker', function (event) {
        flagFilter = true;
        event.stopPropagation();
        var button = $('.js-catalog-clicker');
        // if (button.hasClass('active')) {
        //     button.removeClass('active');
        //     filters.removeClass('active');
        //     setTimeout(function(){
        //         catalog.removeClass('filter');
        //         body.removeClass('fix');
        //     }, 200);
        // } else {
        filters.animate({
            opacity: 1
        }, 200);
        button.addClass('active');
        filters.addClass('active');
        catalog.addClass('filter');
        body.addClass('fix');
        counter = 0;
        letsplay = true;
        windowCoords();
        // }
    });

    $(document).on('click', '.js-filters', function (event) {
        event.stopPropagation();
    });
    $(document).on('click', '.overlay', function (event) {
        var target = $(event.target);
        flagFilter = false;
        event.stopPropagation();
        event.stopImmediatePropagation();
        if (target.is($('.overlay'))) {
            $('.js-catalog-clicker').removeClass('active');
            filters.animate({
                opacity: 0
            }, 200, function (){
                catalog.removeClass('filter');
                body.removeClass('fix');
                filters.removeClass('active upper work');
            });
        }
    })
}

var counter = 0;
var letsplay = true;
function windowCoords() {
    var filter = $('#catalog_filter');
    var myElement = document.getElementById('catalog_filter');
    var elementBox = document.getElementById('filter_box');
    var mc = new Hammer(myElement);
    mc.get('pan').set({ direction: Hammer.DIRECTION_ALL });

    if (letsplay) {
        mc.on("pandown", function(ev) {
            if (elementBox.scrollTop <= 0) {
                if ($('.js-filters').hasClass('upper')) {
                    $('.js-filters').removeClass('upper work');
                    setTimeout(function(){
                        $('.js-filters').removeClass('work');
                        letsplay = true;
                    }, 1000);
                }
                if (letsplay && counter === 0) {
                    counter = counter + 1;
                    $('.js-filters').animate({
                        opacity: '0'
                    }, 400, function (){
                        $('.js-filters').removeClass('work active');
                        $('.js-catalog-clicker').removeClass('active');
                        $('.catalog').removeClass('filter');
                        $('body').removeClass('fix');
                        letsplay = false;
                        flagFilter = false;
                        return false;
                    });
                }
                return false;
            }
            return false;
        });
    }


    mc.on("panup", function(ev) {
        $('.js-filters').addClass('upper work');
        counter = 0;
        setTimeout(function () {
            $('.js-filters').addClass('work');
            letsplay = false;
        }, 1000);
    });

}

function pricesCompiler() {
    var min_before;
    var max_before;
    $(document).on('keydown', '.js-lower-price', function(){
        min_before = $(this).val();
    });
    $(document).on('keydown', '.js-higher-price', function(){
        max_before = $(this).val();
    });
    $(document).on('keyup', '.js-lower-price', function(){
        var lower = $(this);
        var current_min_val = $(this).val();
        if (current_min_val.length <= 1) {
            if (current_min_val == 0) {
                lower.val('');
            }
        } else {
            if (current_min_val > parseInt($('.js-slide-price').data('max'))) {
                lower.val(min_before);
            }
        }
        $('.js-slide-price').slider('values',0,lower.val());
    });

    $(document).on('keyup', '.js-higher-price', function(){
        var higher = $(this);
        var current_max_val = $(this).val();
        if (current_max_val > parseInt($('.js-slide-price').data('max'))) {
            higher.val(max_before);
        }
        $('.js-slide-price').slider('values',1,higher.val());
    });
}

function dotsScroll() {
    $(document).on('click', '.js-paginator-dots', function(){
        $('html, body').animate({scrollTop: $('.catalog__wrapper').position().top - 75}, 500);
    })
}

function mapDetailShower() {
    $(document).on('click','.js-map-shop',function () {
        var item = $(this);
        var index = item.data('text');
        $('.js-shops-mass').addClass('hide');
        $('.chapters__details[data-detail="'+ index +'"]').addClass('active');
        $('html, body').animate({scrollTop: $('.article__left').position().top}, 500)
    });
    $(document).on('click', '.js-detail-close', function () {
        $('.js-shops-mass').removeClass('hide');
        $('.chapters__details').removeClass('active');
    });
}


function showMainMenu() {
    $(document).on('click', '.js-call-menu', function () {
        var button = $(this);
        var menu = $('.js-menu');
        var page = $('.global-wrapper');
        if (button.hasClass('active')) {
            button.removeClass('active');
            menu.animate({
                left: '-200%'
            }, 300);
            page.removeClass('menu__opened');
            page.animate({
                left: '0'
            }, 300);
        } else {
            button.addClass('active');
            // menu.addClass('active');
            menu.animate({
                left: '0'
            }, 300);
            page.addClass('menu__opened');
            page.animate({
                left: '280px'
            }, 300);
            listenMenu();
        }
    })
}

function listenMenu() {
    var myElement = document.getElementsByTagName("body")[0];
    var button = $('.js-call-menu');
    var menu = $('.js-menu');
    var page = $('.global-wrapper');
    var mc = new Hammer(myElement);
    mc.get('pan').set({ direction: Hammer.DIRECTION_ALL });
    mc.on("panleft", function(ev) {
        if (button.hasClass('active')) {
            button.removeClass('active');
            page.removeClass('menu__opened');
            page.animate({
                left: '0'
            }, 300);
            menu.animate({
                left: '-200%'
            }, 300);
        }
        mc.destroy();
        $('body').css('touch-action', 'auto');
    });
    $(document).on('click', '.js-call-menu.active', function () {
        mc.destroy();
        $('body').css('touch-action', 'auto');
    });
    $(document).on('click', '.js-close-menu', function () {
        button.removeClass('active');
        page.removeClass('menu__opened');
        page.animate({
            left: '0'
        }, 300);
        menu.animate({
            left: '-200%'
        }, 300);
        mc.destroy();
        $('body').css('touch-action', 'auto');
    })
}

function formsWorker() {
    $(document).on('click', '.js-get-pass-button', function () {
        $('.js-get-pass-form').removeClass('hidden');
        $('.js-auth-form').addClass('hidden');
    });
    $(document).on('click', '.js-show-authorize-form', function (e) {
        e.preventDefault();
        $.fancybox.open($('#authorization'),{
            afterClose : function( instance, current ) {
                $('.js-get-pass-form').addClass('hidden');
                $('.js-auth-form').removeClass('hidden');
            }
        });
    });
}

function emailChecker() {
    $(document).on('keyup', 'input[type="email"]', function () {
        // var field = $(this).parents('.mainform__item').find('.mainform__error-label');
        var value = $(this).val();
        var pattern = /^[a-z0-9_-]+@[a-z0-9-]+\.[a-z]{2,6}$/i;
        if(value.search(pattern) != 0) {
            $(this).addClass('redborder');
        } else {
            $(this).removeClass('redborder');
        }
    });
}

var flagFilter = false;

function soloSliderInit() {
    var slider = $('.js-solo-slider');
    if (slider.length) {
        slider.slick({
            infinite: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            prevArrow: '<button class="button button__arrow"></button>',
            nextArrow: '<button class="button button__arrow button__arrow--next"></button>'
        })
    }
}

function categoriesSliderInit() {
    $('.categories__slider').slick({
        infinite: false,
        slidesToShow: 8,
        slidesToScroll: 1,
        prevArrow: '<button class="button button__arrow"></button>',
        nextArrow: '<button class="button button__arrow button__arrow--next"></button>',
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 6
                }
            },
            {
                breakpoint: 1023,
                settings: {
                    slidesToShow: 4
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 2
                }
            }
        ]
    })
}

function scrollTo() {
    var $page = $('html, body');
    $('.anchor').click(function () {
        $page.animate({
            scrollTop: $($.attr(this, 'href')).offset().top - 100
        }, 400);
        return false;
    });
}

function mobileSliderInit() {
    $slick_slider = $('.js-mobile-slider');
    settings = {
        mobileFirst: true,
        infinite: false,
        initialSlide: 0,
        slidesToShow: 2,
        slidesToScroll: 1,
        prevArrow: '<button class="button button__arrow"></button>',
        nextArrow: '<button class="button button__arrow button__arrow--next"></button>',
        responsive: [
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 4
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 3
                }
            }
        ]
    };
    if ($(window).width() < 1024) {
        $slick_slider.slick(settings);
    }
    $(window).on('resize', function() {
        if ($(window).width() > 1024) {
            if ($slick_slider.hasClass('slick-initialized')) {
                $slick_slider.slick('unslick');
            }
            return
        }
        if (!$slick_slider.hasClass('slick-initialized')) {
            return $slick_slider.slick(settings);
        }
    });
}

function productSliderInit() {
    var slider = $('.js-product-slider');
    if (slider.length) {
        slider.slick({
            infinite: true,
            slidesToShow: 1,
            prevArrow: '<button class="button button__arrow"></button>',
            nextArrow: '<button class="button button__arrow button__arrow--next"></button>',
            asNavFor: '.js-product-nav',
            focusOnSelect: true

        })
    }

    $('.js-product-nav').slick({
        slidesToShow: 6,
        initialSlide: 0,
        asNavFor: '.js-product-slider',
        focusOnSelect: true,
        responsive: [
            {
                breakpoint: 1100,
                settings: {
                    slidesToShow: 5,
                    prevArrow: '<button class="button button__arrow"></button>',
                    nextArrow: '<button class="button button__arrow button__arrow--next"></button>'
                }
            },
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 6,
                    prevArrow: '<button class="button button__arrow"></button>',
                    nextArrow: '<button class="button button__arrow button__arrow--next"></button>'
                }
            },
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 5,
                    prevArrow: '<button class="button button__arrow"></button>',
                    nextArrow: '<button class="button button__arrow button__arrow--next"></button>'
                }
            },
            {
                breakpoint: 850,
                settings: {
                    slidesToShow: 4,
                    prevArrow: '<button class="button button__arrow"></button>',
                    nextArrow: '<button class="button button__arrow button__arrow--next"></button>'
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 3,
                    prevArrow: '<button class="button button__arrow"></button>',
                    nextArrow: '<button class="button button__arrow button__arrow--next"></button>'
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 2,
                    prevArrow: '<button class="button button__arrow"></button>',
                    nextArrow: '<button class="button button__arrow button__arrow--next"></button>'
                }
            }
        ]
    })
}

function addToCart() {
    $('.button--cart').on('click', function () {
        $(this).addClass('active').find('span').html('Добавлено');
    })
}

function addToFavorites() {
    $('.slide__heart').on('click', function () {
        $(this).toggleClass('active');
    });
    $('.js-favorites').on('click', function (e) {
        e.preventDefault();
        $(this).toggleClass('active');
    });
}

function productMobileSlider() {
    $slick_slider = $('.js-mobile-solo-slider');
    settings = {
        mobileFirst: true,
        infinite: false,
        initialSlide: 0,
        slidesToShow: 1,
        slidesToScroll: 1,
        prevArrow: '<button type="button" class="button button__arrow"></button>',
        nextArrow: '<button type="button" class="button button__arrow button__arrow--next"></button>'
    };
    if ($(window).width() < 768) {
        $slick_slider.slick(settings);
    }
    $(window).on('resize', function() {
        if ($(window).width() > 767) {
            if ($slick_slider.hasClass('slick-initialized')) {
                $slick_slider.slick('unslick');
            }
            return
        }
        if (!$slick_slider.hasClass('slick-initialized')) {
            return $slick_slider.slick(settings);
        }
    });
}

function cabinetMobileSlider() {
    $slick_slider = $('.js-mobile-cabinet-slider');
    settings = {
        mobileFirst: true,
        infinite: false,
        initialSlide: 0,
        slidesToShow: 1,
        slidesToScroll: 1
    };
    if ($(window).width() < 768) {
        $slick_slider.slick(settings);
    }
    $(window).on('resize', function() {
        if ($(window).width() > 767) {
            if ($slick_slider.hasClass('slick-initialized')) {
                $slick_slider.slick('unslick');
            }
            return
        }
        if (!$slick_slider.hasClass('slick-initialized')) {
            return $slick_slider.slick(settings);
        }
    });

    $('.js-cabinet-slider').on('beforeChange', function(event, slick, currentSlide, nextSlide){
        $('.js-cabinet-slider-nav').slick('slickGoTo', nextSlide);
    });
}

function stuffCount(item, count) {
    var x = parseInt(item.parent('.stuff-count').find('.stuff-count__much').val());
    if ((x <= 1) && (count === -1)) {
        item.parent('.stuff-count').find('.stuff-count__much').val(1);
    } else {
        item.parent('.stuff-count').find('.stuff-count__much').val(x + count);
    }
}

function delGoods() {
    $('.goods__del').on('click', function () {
        $(this).parents('.goods__row').remove();
    })
}

function selectDelivery() {
    $('.js-delivery .product__detail-box').on('click',function () {
        $('.js-delivery .product__detail-box').removeClass('selected');
        $(this).addClass('selected');
    })
}

function selectPayment() {
    $('.js-payment .product__detail-box').on('click',function () {
        $('.js-payment .product__detail-box').removeClass('selected');
        $(this).addClass('selected');
    })
}

function selectProfile() {
    $('.js-select-profile').on('click', function () {
        $(this).parents('.cart__profile').addClass('active');
    });
    
    $('.cart__profile .cart__profile-item').on('click',function () {

        var name = $(this).find('.cart__profile-name span').html();

        $('.cart__profile .cart__profile-item').removeClass('selected');
        $(this).addClass('selected');
        $(this).parents('.cart__profile').removeClass('active');
        $('.js-select-profile span').html(name);
    });

    $('.cart__profile-del').on('click', function () {
        $(this).parents('.cart__profile-item').remove();
    })
}

function selectMarket() {
    $('.js-select-market').on('click',function () {
        $('.js-market-block').toggleClass('active');
    })
}

function cartOrderColappse() {
    $('.js-cart-order').on('click', function (e) {
        e.preventDefault();
        $('.cart__order').addClass('active');
        asideStickyInit();
    })
}

function asideStickyInit() {
    if ($(window).width() >= 768) {
        $(".js-sticky-element").stick_in_parent({
            offset_top: 120
        });
    }

    if ($(window).width() >= 1023) {
        $(".js-sticky-element--cart").stick_in_parent({
            offset_top: 120
        });
    }
}

function cartGoodsChecked() {

    var input = $('.cart__checkbox input:checkbox:checked').length;
    var goods = $('.js-goods-item').length;
    var box = $('.cart__selected');
    var mainCheck = $('.js-cart-check');

    if (input > 0) {
        box.addClass('active');
    } else {
        box.removeClass('active');
    }

    if (input === 1) {
        mainCheck.prop('checked', false);
    }

    if (input === goods) {
        mainCheck.prop('checked', 'checked');
    }
}

function cartCheckAll() {
    $(".js-cart-check").click(function(){
        $('.cart__checkbox input:checkbox').not(this).prop('checked', this.checked);
        cartGoodsChecked();
    });
    $('.cart__checkbox input:checkbox').on('click', function () {
        cartGoodsChecked();
    })
}

function deleteSelectedGoods() {
    $('.js-delete-all').on('click', function (e) {
        e.preventDefault();
        $('.cart__checkbox input:checked').parents('.js-goods-item').remove();

    })
}

function favoriteSelectedGoods() {
    $('.js-favorite-all').on('click', function (e) {
        e.preventDefault();
        $('.cart__checkbox input:checked').parents('.js-goods-item').find('.js-favorites').addClass('active');

    })
}

function setCategoriesHeight() {
    var categoriesHeight = $('.nav__content.active').innerHeight();
    $('.nav').css('height', categoriesHeight);
}

function categoriesTabs() {
    $('.nav__tab').on('click', function () {
        if ($(window).width() < 1024) {
            if ($(this).hasClass('active')) {
                $('.nav__tab').removeClass('active');
                $('.nav__content').removeClass('active');
            } else {
                $('.nav__tab').removeClass('active');
                $('.nav__content').removeClass('active');
                $(this).addClass('active').next('.nav__content').addClass('active');
            }
        } else {
            $('.nav__tab').removeClass('active');
            $('.nav__content').removeClass('active');
            $(this).addClass('active').next('.nav__content').addClass('active');
            setCategoriesHeight();
        }
    });
}

function categoriesOpen() {
    $('.js-catalog-open').on('click', function () {
        if ($(window).width() > 1023) {
            setTimeout(function () {
                $('.nav__tab').first().click();
                setCategoriesHeight();
            });
        }
    });
}

function categoriesCollapse() {
    $('.nav__arrow').on('click', function () {
        $(this).toggleClass('active').parents('.nav__item').toggleClass('active');
    })
}




$(window).resize(function () {
    asideStickyInit();
    setCategoriesHeight();
});

$(window).scroll(function () {
    var header = $('.header__nav').offset().top + 25;
    if ($(window).scrollTop() >= header) {
        $('.header__fixed').addClass('fixed');
        $('.global-wrapper').addClass('fixed');
    }
    else {
        if ($(window).scrollTop() == 0)  {
            $('.header__fixed').removeClass('fixed');
            $('.global-wrapper').removeClass('fixed');
        }
    }
    asideStickyInit();
    setCategoriesHeight();
});

$(document).ready(function () {
    emailChecker();
    formsWorker();
    showMainMenu();
    mapDetailShower();
    dotsScroll();
    pricesCompiler();
    catalogDisplayer();
    hiderFilter();
    graysSliderInit();
    rateStarsActivation();
    watchedSliderInit();
    $('.js-catalog-selector').styler({
        selectSmartPositioning: false
    });
    if (flagFilter) {
        return false;
    } else {
        $( window ).resize(function() {
            if ($(window).width() < 767) {
                if (flagFilter) {
                    return false;
                } else {
                    $('.js-filters').removeClass('active').css('opacity', 1);
                    $('.js-catalog-clicker').removeClass('active');
                    $('.catalog').removeClass('filter');
                }
            }
        });
    }
    soloSliderInit();
    categoriesSliderInit();
    scrollTo();
    mobileSliderInit();
    productSliderInit();
    addToCart();
    addToFavorites();
    productMobileSlider();
    delGoods();
    selectDelivery();
    selectPayment();
    selectProfile();
    selectMarket();
    cartOrderColappse();
    cartCheckAll();
    deleteSelectedGoods();
    favoriteSelectedGoods();
    cabinetMobileSlider();
    categoriesTabs();
    categoriesOpen();
    categoriesCollapse();
});